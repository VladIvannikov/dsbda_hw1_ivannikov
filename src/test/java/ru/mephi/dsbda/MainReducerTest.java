package ru.mephi.dsbda;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * MainReducerTest.java
 * MainReducer Test class.
 */
public class MainReducerTest {

    private ReduceDriver<LogStringData, IntWritable, Text, IntWritable> reduceDriver;
    List<IntWritable> values;
    /**
     * Creates reduceDriver before each try.
     */
    @Before
    public void setUp() {
        reduceDriver = ReduceDriver.newReduceDriver(new MainReducer());
        values = new ArrayList<IntWritable>();
        values.add(new IntWritable(1));
        values.add(new IntWritable(12));
        reduceDriver
                .withCacheFile("./input/city.en.txt")
                .withCacheFile("./input/region.en.txt");
    }

    /**
     * Correct Reducer test (cityId from city file).
     * @throws IOException
     */
    @Test
    public void CorrectReducerCityTest() throws IOException {
        reduceDriver
                .withInput(
                        new LogStringData(6, "Linux"),
                        values
                );
        reduceDriver.withOutput(new Text("City:  qinhuangdao  OS:   Linux"), new IntWritable(13));
        reduceDriver.runTest();
    }

    /**
     * Correct Reducer test (cityId from region file).
     * @throws IOException
     */
    @Test
    public void CorrectReducerRegionTest() throws IOException {
        reduceDriver
                .withInput(
                        new LogStringData(106, "Linux"),
                        values
                );
        reduceDriver.withOutput(new Text("City:  anhui  OS:   Linux"), new IntWritable(13));
        reduceDriver.runTest();
    }

    /**
     * Проверка на выход данных без указанного города
     * @throws IOException
     */
    @Test
    public void MissingCityCorrectReducerTest() throws IOException {
        reduceDriver
                .withInput(
                        new LogStringData(666, "Linux"),
                        values
                );
        reduceDriver.withOutput(new Text("Noname #666    OS:    Linux"), new IntWritable(13));
        reduceDriver.runTest();
    }
}
