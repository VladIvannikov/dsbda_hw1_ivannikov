package ru.mephi.dsbda;

import java.io.IOException;


import eu.bitwalker.useragentutils.UserAgent;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

/**
 * MainMapper.java
 * Mapper class.
 */
public class MainMapper extends Mapper<Object, Text, LogStringData, IntWritable> {


    private final static int CITY_ID_OFFSET = 17;   // From Table 3 ipinyou-dataset.pdf
    private final static int PRICE_OFFSET = 4;      // From Table 3 ipinyou-dataset.pdf
    private final static int USER_AGENT = 20;       // From Table 3 ipinyou-dataset.pdf
    private final static int MIN_PRICE = 250;       // From task
    private final static IntWritable one = new IntWritable(1);




    /**
     * Mapper method
     * Парсим входную строку ивыделям полезные данные с присвоением единицы( LogStringData(CityId,osName), 1). Отсекаем данные у которых Price меньше    250
     */
    @Override
    public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
        String[] tokens = value.toString().split("\\t+");
        try {
            if (tokens.length >20) {
                int price = Integer.parseInt(tokens[tokens.length - PRICE_OFFSET]);
                if (price < MIN_PRICE) {
                    return;
                }

                int cityId = Integer.parseInt(tokens[tokens.length - CITY_ID_OFFSET]);
                String token = tokens[tokens.length-USER_AGENT];
                String osName = UserAgent.parseUserAgentString(token).getOperatingSystem().getName();

                if (!osName.equalsIgnoreCase("UNKNOWN")) {
                    LogStringData data = new LogStringData(cityId, osName);
                    context.write(data, one);
                }
                else{
                    context.getCounter(CounterType.Malformed).increment(1);
                }
            }
            else {
                if (tokens.length > 2 )
                {
                    context.getCounter(CounterType.Malformed).increment(1);
                }
            }

        } catch (NumberFormatException ex) {
            // Wrong string. Nothing to do here.
        }
    }
}
