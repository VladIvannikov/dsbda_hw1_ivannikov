package ru.mephi.dsbda;


import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Counter;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import java.net.URI;
import java.util.logging.Logger;

/**
 * MainCounter.java
 *
 */
public class MainCounter {

    /**
     * точка входа.
     * args входные аргументы
     * Если аргументов меньше 2, выбрасываем runtimeException
     */
    public static void main(String[] args) throws Exception {


        if (args.length < 4) {
            throw new RuntimeException("illeagal parametrs");
        }

        Logger log = Logger.getLogger(MainCounter.class.getName());
        /// Конфигурация
        Configuration conf = new Configuration();

        // создаем job
        Job job = Job.getInstance( conf, "BDA HW1 Counter" );

	    // Редьюсеры
	    job.setNumReduceTasks(15);

        job.setJarByClass(MainCounter.class);
        job.setMapperClass(MainMapper.class);
        job.setReducerClass(MainReducer.class);
        job.setPartitionerClass(MainPartitioner.class);

        // Mapper
        job.setMapOutputKeyClass(LogStringData.class);
        job.setMapOutputValueClass(IntWritable.class);

        // Выходные (Reducer)
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        // Формат - seq(SequenceFileOutputFormat.class);
        job.setOutputFormatClass(SequenceFileOutputFormat.class);


        // Входные и Выходные пути к данным, берем их cmd аргументов
        FileInputFormat.addInputPath(job, new Path(args[1]));
        FileOutputFormat.setOutputPath(job, new Path(args[2]));

        // Кэшируем файлы для Reduce side join
        job.addCacheFile(new URI(args[3]));
        job.addCacheFile(new URI(args[4]));
        //job.addCacheFile(new URI("./input/city.en.txt"));
        //job.addCacheFile(new URI("./input/region.en.txt"));

        long startTime = System.nanoTime();
        //Выполнение
        log.info("==================================JOB START========================");
        job.waitForCompletion(true);
        long estimatedTime = System.nanoTime() - startTime;
        log.info("Spent Time:   " + estimatedTime);
        Counter counter = job.getCounters().findCounter(CounterType.Malformed);
        log.info("=====================COUNTERS " + counter.getName() + ": " + counter.getValue() + "=====================");
        log.info("==================================JOB END========================");
    }
}
