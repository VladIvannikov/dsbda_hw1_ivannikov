package ru.mephi.dsbda;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Partitioner;

/**
 * MainPartitioner.java
 * Custom partitioner class (by OperationSystemType).
 */
public class MainPartitioner extends Partitioner<LogStringData, IntWritable> {

    public int getPartition(LogStringData logStringData, IntWritable intWritable, int i) {
            return (logStringData.hashCode() & Integer.MAX_VALUE) % i ;

    }
}
