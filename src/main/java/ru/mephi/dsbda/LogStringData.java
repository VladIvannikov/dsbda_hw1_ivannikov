package ru.mephi.dsbda;

import org.apache.hadoop.io.WritableComparable;


import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * LogStringData.java
 * Класс с полями ид-города и именем ОС
 */
public class LogStringData implements WritableComparable<LogStringData> {

    private int cityId;
    private String osName;

    // constructors
    public LogStringData() {
        cityId = -1;
        osName = null;
    }

    public LogStringData(int cityId, String osName) {
        this.cityId = cityId;
        this.osName = osName;
    }

    //getters
    public int getCityId() {
        return cityId;
    }

    public String getOsName() {
        return osName;
    }



    @Override
    public boolean equals(Object object) {
        if ((object == null) || (!(object instanceof LogStringData))) {
            return false;
        }
        return this.cityId == ((LogStringData) object).cityId  && this.osName.equalsIgnoreCase(((LogStringData)object).osName);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((osName != null) ? osName.hashCode() : (0));
        return result;
    }

    @Override
    public String toString() {
        return "LogString: " + this.cityId + ";    OsName:  " + this.osName + ";";
    }

    public int compareTo(LogStringData o) {
        return this.cityId < o.cityId ? -1 : (this.cityId == o.cityId ? 0 : 1);
    }

    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeInt(cityId);
        dataOutput.writeUTF(osName);
    }

    public void readFields(DataInput dataInput) throws IOException {
        cityId = dataInput.readInt();
        osName = dataInput.readUTF();
    }
}
