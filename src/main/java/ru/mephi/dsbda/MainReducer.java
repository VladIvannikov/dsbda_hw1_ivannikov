package ru.mephi.dsbda;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.HashMap;

/**
 * MainReducer.java
 * Reducer class.
 */
public class MainReducer extends Reducer<LogStringData, IntWritable, Text, IntWritable> {

    private HashMap<Integer, String> cities = new HashMap<Integer, String>();

    /**
     * Метод Setup.
     * Создаем словарь имен городов из закешированных файлов.
     */
    @Override
    public void setup(Context context) throws IOException {
        for (URI fileUri : context.getCacheFiles()) {
            if (fileUri.getPath().toLowerCase().contains("city") ||
                    fileUri.getPath().toLowerCase().contains("region")) {
                BufferedReader reader =
                        new BufferedReader(
                                new InputStreamReader(
                                        FileSystem.get(context.getConfiguration())
                                                .open(new Path(fileUri))));
                try {
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        String[] tokens = line.split("\\s+");
                        cities.put(Integer.parseInt(tokens[0]), tokens[1]);
                    }
                }
                catch (NullPointerException ex) {
                    // Incorrect string in cities file.
                    // Nothing to do here.
                }
                finally {
                    reader.close();
                }
            }
        }
    }

    /**
     * Reducer main метод.
     * суммирует все единицы полученные  от MainPartitioner ( город/ос(фиксированный ос)), выдает суммарное количество по городу
     */
    @Override
    public void reduce(LogStringData key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
        int sum = 0;
        for (IntWritable value : values) {
            sum += value.get();
        }

        IntWritable temp = new IntWritable(sum);

        Text text = null;
        if (cities.containsKey(key.getCityId())) {
            text = new Text("City:  "   +   cities.get(key.getCityId()) + "  OS:   "    +   key.getOsName());
        }
        else {
            text = new Text("Noname #" + key.getCityId() + "    OS:    " + key.getOsName());
        }

        context.write(text, temp);
    }
}
